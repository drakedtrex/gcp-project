provider "google" {
  credentials = file("vareza-labs-85581afdfe12.json")
  project = "vareza-labs"
  region  = "asia-southeast2"
}

# https://www.terraform.io/language/settings/backends/gcs
terraform {
  backend "gcs" {
    bucket = "vareza-tf-state-staging"
    prefix = "terraform/state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.0"
    }
  }
}